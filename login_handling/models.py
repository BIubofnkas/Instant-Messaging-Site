# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    theme_color = models.CharField(max_length=5, blank=True)
    accent_color = models.CharField(max_length=8, blank=True)



class sent_messages(models.Model):
    sender = models.TextField(blank=True)
    message = models.TextField(blank=True)
    reciever = models.TextField(blank=True)

class recieved_messages(models.Model):
    reciever = models.TextField(blank=True)
    message = models.TextField(blank=True)
    sender = models.TextField(blank=True)

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

    instance.profile.save()
