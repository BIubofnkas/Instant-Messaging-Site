from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^main/$', views.main_base, name='main_base'),
    url(r'^main/user_settings/$', views.user_settings, name='user_settings'),
    url(r'^main/change_theme/$', views.change_theme, name='change_theme'),
    url(r'^main/save_sent_message/$', views.save_sent_message, name='save_sent_message'),
    url(r'^upload/$', views.upload , name='upload'),
    
    ]
